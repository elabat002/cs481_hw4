import 'package:flutter/material.dart';
import 'package:flutter/animation.dart';

class AnimatedSloth extends AnimatedWidget {
  AnimatedSloth({Key key, Animation<double> animation}) : super(key: key, listenable: animation);

  @override
  Widget build(BuildContext context) {
    final animation = listenable as Animation<double>;
    return Center(
      child: Container(
        width: animation.value,
        height: animation.value,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage('./assets/images/sloth.png')
          )
        ),
      )
    );
  }

}