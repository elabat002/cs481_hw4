import 'package:flutter/material.dart';
import 'package:flutter/animation.dart';
import 'package:flutter_app/AnimatedSloth.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> with TickerProviderStateMixin{
  Animation<double> _resizeAnimation;
  AnimationController _resizeController;
  Animation<Offset> _slideAnimation;
  AnimationController _slideController;
  AnimationController _iconResizeController;
  AnimationController _iconSlideController;
  bool _pause = true;
  bool _slide = false;

  @override
  void initState () {
    super.initState();
    _iconResizeController = AnimationController(duration: Duration(milliseconds: 300), vsync: this);
    _iconSlideController = AnimationController(duration: Duration(milliseconds: 300), vsync: this);
    _resizeController = AnimationController(duration: const Duration(seconds: 2), vsync: this);
    _resizeAnimation = Tween<double>(begin: 250, end: 0).animate(_resizeController)
      ..addStatusListener((status) {
        if (status == AnimationStatus.completed) {
          if (_pause == false)
            _resizeController.reverse();
        } else if (status == AnimationStatus.dismissed) {
          if (_pause == false)
            _resizeController.forward();
        }
      });
    _slideController = AnimationController(duration: const Duration(seconds: 3), vsync: this);
    _slideAnimation = Tween<Offset>(begin: Offset.zero, end: const Offset(1.5, 0.0)).animate(CurvedAnimation(
      parent: _slideController,
      curve: Curves.elasticIn,
    ))..addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        _slideController.reverse();
        _iconSlideController.reverse();
      }
    });
  }

  void _handleOnPressed() {
    setState(() {
      _pause = !_pause;
      if (_pause == false) {
        _iconResizeController.forward();
        _resizeController.forward();
      } else {
        _iconResizeController.reverse();
        _resizeController.reverse();
      }
    });
  }

  void _handleSlideOnPressed() {
    setState(() {
      _slide = ! _slide;
      _iconSlideController.forward();
      _slideController.forward();
    });
  }

  @override
  void dispose() {
    super.dispose();
    _resizeController.dispose();
    _slideController.dispose();
    _iconResizeController.dispose();
    _iconSlideController.dispose();
  }

  @override
  Widget build(BuildContext context){
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('Homework 4 | Willy the cutie sloth'),
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SlideTransition(
                    position: _slideAnimation,
                    child: Container(
                      width: 300,
                      height: 300,
                      child: AnimatedSloth(animation: _resizeAnimation)
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Column(
                        children: [
                          Text("Resize Willy !"),
                          ClipOval(
                            child: Container(
                              color: Colors.lightBlue,
                              child: IconButton(
                                color: Colors.white,
                                iconSize: 30,
                                icon: AnimatedIcon(
                                  icon: AnimatedIcons.play_pause,
                                  progress: _iconResizeController,
                                ),
                                onPressed: () => _handleOnPressed(),
                              ),
                            ),
                          ),
                        ],
                      ),
                      Column(
                        children: [
                          Text("Willy wanna swing !"),
                          ClipOval(
                            child: Container(
                              color: Colors.lightBlue,
                              child: IconButton(
                                color: Colors.white,
                                iconSize: 30,
                                icon: AnimatedIcon(
                                  icon: AnimatedIcons.play_pause,
                                  progress: _iconSlideController,
                                ),
                                onPressed: () => _handleSlideOnPressed(),
                              ),
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                ],
              ),
        )
        )
      );
  }
}